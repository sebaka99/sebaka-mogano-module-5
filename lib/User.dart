// ignore: file_names
class User {
  String? id;
  String? name = "";
  String? password = "";
  String? email = "";
  String? lastname = "";

  User(
      {this.id = "",
      required this.name,
      required this.password,
      required this.email,
      required this.lastname});
  Map<String, dynamic> toJson() =>
      {'id': id, 'Firstname': name, 'email': email, 'lastname': lastname};
}
